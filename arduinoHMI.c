//**************************************************************************
// Author: Nick Christensen
// Date:   April 2019
//**************************************************************************

#include "SerialMP3Player.h"
#include "ros.h"
#include <std_msgs/String.h>
#include <Adafruit_NeoPixel.h>
#include "SerialMP3Player.h"

#define TX 11	// TX pin for sound board
#define RX 10	// RX pin for sound board

SerialMP3Player mp3(RX,TX);	// Initialize sound board serial connection object

int soundSelect = -1;   // Keeps track of what sound we should be playing next
int currentState = -1;  // Keeps track of whether we are in autonomous mode or not

/* Setup LED */
#define LED_PIN     6
#define BRIGHTNESS 50
#define LED_COUNT  60

Adafruit_NeoPixel strip(LED_COUNT, LED_PIN, NEO_GRBW);  // Initialize LED strip object

/* ROS Node Handle */
ros::NodeHandle nh;

byte * Wheel(byte WheelPos) {
  static byte c[3];
  
  if(WheelPos < 85) {
   c[0]=WheelPos * 3;
   c[1]=255 - WheelPos * 3;
   c[2]=0;
  } else if(WheelPos < 170) {
   WheelPos -= 85;
   c[0]=255 - WheelPos * 3;
   c[1]=0;
   c[2]=WheelPos * 3;
  } else {
   WheelPos -= 170;
   c[0]=0;
   c[1]=WheelPos * 3;
   c[2]=255 - WheelPos * 3;
  }

  return c;
}

/* This function manages the LED effect */
void rainbowCycle(int SpeedDelay) {
  byte *c;
  uint16_t i, j;

  for(j=0; j<256*5; j++) { // 5 cycles of all colors on wheel
    for(i=0; i< LED_COUNT; i++) {
      c=Wheel(((i * 256 / LED_COUNT) + j) & 255);
      strip.setPixelColor(i, *c, *(c+1), *(c+2));
    }
    strip.show();
    delay(SpeedDelay);
  }
}

// --------------------------------------------------------------
// Ros topic handler
//
//	Currently we receive a boolean value for the autonomous state
// --------------------------------------------------------------
void messageCb(const std_msgs::bool& msg)
{
	if (prevState != *msg)
	{
		currentState = *msg;
		soundSelect = *msg;
	}
}

ros::Subscriber<std_msgs::bool> sub("/eps_controller/eps_controller/request_autonomous/status", &messageCb);

void setup()
{
	strip.begin();                   // INITIALIZE NeoPixel strip object
    strip.show();                    // Turn OFF all pixels ASAP
    strip.setBrightness(BRIGHTNESS); // Set BRIGHTNESS to about 1/5 (max = 255)
	
	delay(500);                      // Make sure sound board has booted
	Serial.begin(9600);
	mp3.begin(9600);
	delay(500);

	sendCommand(CMD_SEL_DEV, DEV_TF);  // Send SD card select command to soundboard
	delay(500);
  
	// Initialize ros subscriber
	nh.initNode();
	nh.subscribe(sub);
}

void loop()
{
	// This call triggers processing of ros topics and may need
	// to be called more often if sound delay is an issue
	nh.spinOnce();

	// Play the corresponding sound
	switch (soundSelect)
	{
		case 0:
		  mp3.play(1);
		  soundSelect = -1;
		  prevState = currentState;
		  break;
		case 1:
		  mp3.play(2);
		  soundSelect = -1;
		  prevState = currentState;
		  break;
	}

	// Check for message from sound board
	if (mp3.available())
	{
	  Serial.println(decodeMP3Answer());
	}
	
	// Process LED effect
	rainbowCycle(20);
}
